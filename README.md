This page was inspired by Chris Maurer's README.

### Brian's README
My name is Brian Piscitelli and I am a Federal Solutions Architect working with the IC.

*  [LinkedIn](https://www.linkedin.com/in/brian-piscitelli-53898a1/)
*  [GitLab](https://gitlab.com/bpiscitelli)
*  [GitLab Team Page](https://about.gitlab.com/company/team/#bpiscitelli)

If we are working together, or even if we aren't, here are some things it might be helpful to know about me:

General

*  I've worked in technology for my entire career (15+ years) and 95% of that has been spent on Public Sector.
*  I have spent time as an FTE working for several IC agencies.
*  I was prior US Navy and departed as an IT2 (E5).  I was stationed in Keflavik Iceland for 2.5 years and then assigned to USS Iwo Jima (LHD-7) aka Gator Freighter for another 8 months.  If you want me to talk your ear off, ask me how my trip to meet the ship in Italy went.  
*  I have spent 4 years of my career as an admin, 8 years doing Professional Services work, and 4 years as a Sales Engineering/Solutions Architect. 
*  I've worked with a lot of technologies in my career: Networking, OS deployment/Management, Virtualization, Lots of Cyber Security and Cross-Domain.  
*  I consider myself very easy to get along with and easy to work with.  When it comes to working with me just explain why something needs to get done.  It helps me to understand the bigger picture. 
*  I take pride in my work and if something is not up-to-par by your standards then please let me know so that we can resolve. 
*  When working with SALs, I am here to support!  I believe in teamwork and working together.  Generally speaking, I prefer the SAL to be the "Quarterback" and discuss the plays with me and then we execute.     
*  Outside of work I am a very busy person.  I have 2 children - 1 daughter Mila (10mo) and 1 son Blake (2 yrs) as of 2019.  In my spare time without the kids, you can find me: playing video games, doing car stuff (modding, research, etc), traveling the world with my wife Mindy, Outdoor activities, and photography.  Oh and can’t forget home renovations (sometimes by choice).
*  I am a big BMW fan and have done many, many, many modifications and work on my e46 M3, Kia Optima and plans for my F10 M5.
*  I have a professional photography page: http://imagebp.com/ where I have a lot of my travel photos posted.  I'm still working on several other countries. 


### Logistics

*  My morning can get a little crazy trying to get the kids to daycare.  I will tend to block off my calendar in the mornings to provide preparation time for morning meetings.  My core hours are between 9:30 and 5:30 EST, but tend to jump back on after the kids have settled down from daycare (7pm-9pm).
*  If you are planning meetings with me please take into account I am EST. 
*  If you are planning in-person meetings please take into account that I have extremely bad traffic during rush hours (One of the worst commutes in the entire USA).  If it is possible to try to schedule in-person meetings for the late morning (11am).  
*  I like to plan for things so please try to provide details of events ASAP. 

### Communication

*  When you need to reach me, here are my communication priorities and preference: 
    *  ***NOTE: I will write this using the [Eisenhower Matrix](https://www.eisenhower.me/eisenhower-matrix/)***
    *  Urgent and Important:  Phone Call and Email or Issue - Please don't hesitate to call me if there is something urgent and important.  I want to take care of these ASAP but sometimes I'm working on other things and may not get the messages promptly.
    *  Less Urgent and Important: Slack generally work well 
    *  Urgent and Less Important: Slack with an email or Issue.
    *  Less Urgent and Less Important:  Email, Tag in Slack. 
*  In their respective order from the highest priority of communication is: Phone, Text, Slack, Email, Gitlab Issues
*  If you call and I don't answer, please leave a voicemail. I do check them! :-)

    *   Slack - If I'm working I try to keep Slack open on the side. For small group discussions, never hesitate to create a dedicated channel.  DMs are fine, but if it involves more than me, having a dedicated channel might be more helpful.
    *   Email - I check email throughout the day, but email is my least preferred mode of communication.
    *   Text - My phone is always with me and I will respond to texts during both working and non-working hours (but expect some response delays during non-working hours)
    *   Phone - Generally, I will let my phone go to voicemail if I'm in the middle of something. I only return calls that leave voicemails.
    *   GitLab Issues - this is a great tool to use for your workload.  Each customer you speak with should eventually have a project created with issues associated with tasks for that customer.

*  I prefer to AVOID EMAIL for the following activities:
    *  Long dialog requiring discussion - better to create an issue, start a conversation on slack or set up a meeting
    *  Requests for review - better to create an issue, or start a slack conversation and ping the required reviewers
    *  I consider Slack to be asynchronous. If there is a timeline for a request you might have please specify it.


### Teams I Support
* I am mainly focused on supporting the Intelligence Community but I am available to assist with anything DOD related. 

### Updates
*  This page will always be a work in progress.  If there are changes you suggest, I am accepting Merge Requests.
